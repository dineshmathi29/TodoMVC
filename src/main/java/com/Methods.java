package com;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.*;

public class Methods{
	public WebDriver driver;
	public static String url;
	//AddTodo
	public static String addToDo[];
	public static String inputBox;
	
	
	//CompleteTodo
	public static String CompleteToDo[];
	public static String markAsComplete;
	public static String todoLabel;
	public static String itemLeftCount;
	public static String completedItem;
	
	//Delete Todo
	public static String deleteToDo[];
	public static String deleteButton;
	public static String deleteItem;
	
	//Filter Todo
	public static String addItems[];
	public static String completeItems[];
	public static String readItems;
	public static String active;
	public static String completed;
	public static String filterAvailable;
	
	public Actions actions;
	
	List<WebElement> toDoList = new ArrayList<WebElement>();
	
	public void readObjectRepository() throws IOException {
		File file = new File("./src/main/resources/todoObjects.properties");
		FileInputStream fis = new FileInputStream(file);
		Properties pr = new Properties();
		pr.load(fis);
		url=pr.getProperty("url");
		//Add Todo
		inputBox=pr.getProperty("inputBox");
		addToDo=pr.getProperty("addToDo").split(",");
		todoLabel=pr.getProperty("todoLabel");
		
		//Complete Todo
		markAsComplete=pr.getProperty("markAsComplete");
		CompleteToDo=pr.getProperty("CompleteToDo").split(",");
		itemLeftCount=pr.getProperty("itemLeftCount");
		completedItem=pr.getProperty("completedItem");
		
		//Delete Todo
		deleteButton=pr.getProperty("deleteButton");
		deleteToDo=pr.getProperty("deleteToDo").split(",");
		deleteItem=pr.getProperty("deleteItem");
		
		//filter Todo
		addItems=pr.getProperty("addItems").split(",");
		completeItems=pr.getProperty("completeItems").split(",");
		readItems=pr.getProperty("readItems");
		filterAvailable=pr.getProperty("filterAvailable");
		active=pr.getProperty("active");
		completed=pr.getProperty("completed");		
		fis.close();
	}
	
	//To open the browser with the given URL
	public void openDriver() {
		System.setProperty("webdriver.chrome.driver", "./Driver/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get(url);
		driver.manage().window().maximize();
		actions = new Actions(driver);
	}
	
	//Method to Add items to the Todo list
	public void addItem(String todoItemName) {
		Assert.assertFalse(todoItemName.trim().isEmpty(),"please enter valid value to add a todo item. Entered value is blank");
		int sizeBeforeAdd=driver.findElements(By.xpath(todoLabel)).size();
		driver.findElement(By.xpath(inputBox)).sendKeys(todoItemName);
		driver.findElement(By.xpath(inputBox)).sendKeys(Keys.ENTER);
		List<WebElement> arrList = new ArrayList<WebElement>();
		arrList=driver.findElements(By.xpath(todoLabel));
		int sizeAfterAdd = arrList.size();
		Assert.assertEquals(sizeAfterAdd-sizeBeforeAdd,1,"To do item is not added");
		String newToDo=arrList.get(sizeAfterAdd-1).getText();
		Assert.assertEquals(newToDo, todoItemName,"To do item is not added");
	}
	
	//Method to mark as Complete the item in Todo list
	public void completeItem(String todoItemName) {
		Assert.assertFalse(todoItemName.trim().isEmpty(),"please enter value to mark as complete for a todo item");
		Assert.assertEquals(driver.findElements(By.xpath(markAsComplete.replace("CompleteToDo",todoItemName))).size(), 1,"ToDo item is not present in the list");
		WebElement checkBox= driver.findElement(By.xpath(markAsComplete.replace("CompleteToDo",todoItemName)));
		int itemLeftBefore=Integer.parseInt(driver.findElement(By.xpath(itemLeftCount)).getText());
		Assert.assertTrue(!checkBox.isSelected(),"To Do item is already marked as Completed");
		checkBox.click();
		int itemLeftAfter=Integer.parseInt(driver.findElement(By.xpath(itemLeftCount)).getText());
		Assert.assertTrue(checkBox.isSelected(), "ToDo item is present and its not marked as Completed");
		Assert.assertEquals(driver.findElement(By.xpath(completedItem.replace("CompleteToDo", todoItemName))).getCssValue("text-decoration-line").trim(),"line-through","Item is marked as completed but not item is crossed out");
		Assert.assertEquals(itemLeftBefore-itemLeftAfter,1,"Item Left count is not updated properly");
	}
	
	//Method to delete the item from Todo list
	public void deleteItem(String todoItemName) { 
		Assert.assertFalse(todoItemName.trim().isEmpty(),"please enter value to delete a todo item");
		Assert.assertEquals(driver.findElements(By.xpath(deleteButton.replace("deleteToDo",todoItemName))).size(),1,"ToDo item is not present in the list");
		WebElement removeButton= driver.findElement(By.xpath(deleteButton.replace("deleteToDo",todoItemName)));
		actions.moveToElement(driver.findElement(By.xpath(deleteItem.replace("deleteToDo",todoItemName)))).perform();
		removeButton.click();
		Assert.assertEquals(driver.findElements(By.xpath(deleteButton)).size(),0,"ToDo item is not deleted");
	}
	
	//Method to Filter Items based on FilterType 
	public void filterItem(String FilterType) {
		String filterXpath = null;
		String labelStatus = null;
		if(FilterType.equals("Active")) {
			labelStatus="none";
			filterXpath=active;
		}else if(FilterType.equals("Completed")) {
			labelStatus="line-through";
			filterXpath=completed;
		}
		driver.findElement(By.xpath(filterXpath)).click();
		toDoList =driver.findElements(By.xpath(readItems));
		for (WebElement el : toDoList){
			Assert.assertEquals(el.getCssValue("text-decoration-line"),labelStatus,"Filter results are not as expected");
		}
	}
	
}

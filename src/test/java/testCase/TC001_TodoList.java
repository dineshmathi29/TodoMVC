package testCase;

import java.io.IOException;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import com.Methods;

public class TC001_TodoList extends Methods{

@BeforeSuite
public void openBrowser() throws IOException {
	readObjectRepository();
	openDriver();
}
	
@AfterSuite
public void driverQuit(){
	
	driver.quit();
}

@Test(priority=0)
void addTodo() throws InterruptedException {
	
	for (String st : addToDo) {
		addItem(st);
	}
}
@Test(priority=1)
void completeTodo() {
	
	for (String st : CompleteToDo) {
		completeItem(st);
	}
}

@Test(priority=2)
void deleteTodo() { 
	for (String st : deleteToDo) {
		deleteItem(st);
	}
}

@Test(priority=3)
void filterTodo(){
	
	for (String st : addItems) {
		addItem(st);
	}
	for (String st : completeItems) {
		completeItem(st);
	}
	Assert.assertFalse(driver.findElements(By.xpath(filterAvailable)).size()==0,"Filters are Not displayed");
	filterItem("Active");
	filterItem("Completed");
}
}

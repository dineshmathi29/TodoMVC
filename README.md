Instruction notes before running this project
1. Fill in the input data in the todoObjects Properties File palced in the path - /src/main/resources/
2. xpaths are placed in object repository file - todoObjects Properties
3. All the 4 testcases can be tested with multiple input values
4. Multiple values can be given by seperating the values by comma for the below input fields of Properties File
    1.AddTodo:- for ex: addToDo= Manual Testing,Selenium,TestNG,Java
    2.CompleteToDo:- 
    3.deleteToDo
    4.addItems
    5.completeItems
5. once filled the input details, Run the project as TestNG suite
